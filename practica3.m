function practica3
  close all
  pkg load signal
  #primera parte
  display("primera parte")
  x=8;
  f=100+x
  sr=8000;
  hold on
  n=1; #posicion inicial en el subplot
  for duracion=[1,0.5,0.1,0.05]   #duraciones de las sennales indicadas en la practica
    #correlaciones
    t=0:1/sr:duracion;  #secuencia de tiempo
    sig=cos(2*pi*t*f);  #sennal en el dominio del tiempo
    fref=sr*t/duracion; #secuencia de escalas para la transformada
    tabla=[];           #tabla que se llenara con la frecuencia y el maximo de la correlacion
    for fr=100:110  #valores de frecuencia para probar la correlacion
      refsig=cos(2*pi*t*fr); #sennal con la que se va a comparar
      mc=max(xcorr(sig,refsig)); #maximo de la correlacion
      tabla=[tabla;[fr,mc]]; #agrega el maximo de la correlacion y la frecuencia probada a la tabla
    endfor 
    display(" ")
    display(duracion) #muestra la duracion de la sennal
    display(tabla)  #muestra la tabla con los resultados para esta duracion
    #transformadas
    EA=abs(fft(sig)); #obtiene el espectro de amplitud
    subplot(2,2,n), stem(fref,EA) #grafica el espectro de amplitud
    A=max(EA); #obtiene el valor maximo del espectro de amplitud
    m=0; #inicializa la posicion en la secuencia de frecuencias
    HEA=EA(1:length(EA)/2); #divide el espectro en dos, solo se necesita la mitad
    for i=HEA #busca en la mitad del espectro
      m=m+1; #aumenta el indicador de posicion
      if i==A #si el valor de la posicion en el espectro es igual al maximo
        frecuencia=fref(m); #esta es frecuencia de amplitud maxima
      endif
    endfor
    display(frecuencia)  #frecuencia obtenida con la transformada
    n=n+1;
  endfor
  #segunda parte
  display(" ")
  display(" ")
  display(" ")
  display(" ")
  display("segunda parte")
  fal=floor(rand(1)*250+250); #crear una frecuencia entera aleatoria entre 250 y 500
  sr=4000;
  duracion=1/10;
  t=0:1/sr:duracion; #secuencia de teimpo de la sennal
  sig=cos(2*pi*t*fal); #sennal en el dominio del tiempo
  fref=sr*t/duracion; #secuencia de frecuencias para la transformada de la sennal
  EA=abs(fft(sig)); #espectro de amplitud
  figure, stem(fref,EA) #graficar el espectro de amplitud
  A=max(EA); #maxima amplitud del espectro
  m=0;
  HEA=EA(1:length(EA)/2);
  for i=HEA  #encontramos la frecuencia maxima igual que en la primera parte 
    m=m+1;
    if i==A
      frecuencia=fref(m);
    endif
  endfor
  display(fal) #frecuencia aleatoria real que generamos
  display(frecuencia)  #frecuencia encontrada con la transformada
  tabla=[]; #iniciar con una tabla vacia
  for fr=floor(fal-5:fal+5) #probar con las frecuencias que se piden en la practica
    refsig=cos(2*pi*t*fr);  #sennal para correlacionar
    mc=max(xcorr(sig,refsig)); #maximo de la correlacion
    tabla=[tabla;[fr,mc]]; #agregar campo a la tabla con la frecuencia y la correlacion maxima
  endfor
  display(tabla) #mostrar la tabla
endfunction
