pkg load signal #Solo en Octave
sr=44100; # frecuencia de muestreo estandar 44.1KHz
f=1200; # frecuencia de la sennal muestreada
t=0:1/sr:0.02;
#Generamos la sennal sin filtrar
sig1=cos(2*pi*f*t);#Expresion de la sennal muestreada
#Aqui generamos un filtro Butterworth de cuarto orden
fc=1000; # frecuencia de corte
orden=4; # orden del filtro, 4to orden
[a,b]=butter(orden,2*fc/sr); # genera parametros del filtro
#Filtramos la sennal
sig2=filter(a,b,sig1); # sennal filtrada, la funcion filter recibe los parametros del filtro y la sennal a filtrar
#Graficamos ambas sennales
hold on
plot(sig1)
plot(sig2)
#Voltajes pico a pico
vppsig1=max(sig1)-min(sig1)
vppsig2=max(sig2)-min(sig2)