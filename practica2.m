#Primera parte
pkg load signal
X=8
F=X*100;
fs1=20000;
fs2=2000;
fs3=4000;
fs4=7000;
fs5=10000;
sig1=sin(2*pi*F*(0:1/fs1:2));
sig2=resample(sig1,fs2,fs1);#Este es ep procedimiento para remuestrear
sig3=resample(sig2,fs3,fs2);
sig4=resample(sig2,fs4,fs2);
sig5=resample(sig2,fs5,fs2);
tabla=[length(sig1)/fs1,length(sig2)/fs2,length(sig3)/fs3,length(sig4)/fs4,length(sig5)/fs5;length(sig1),length(sig2),length(sig3),length(sig4),length(sig5)]#Asi se miden las duraciones y el N de las sennales
A1=-trapz(abs(sig1),0:1/fs1:2) #Les ponemos valor absoluto para que no sean cero
A2=-trapz(abs(sig2),0:2/length(sig2):2-2/length(sig2))#Son integrales por el metodo del trapecio
A3=-trapz(abs(sig3),0:2/length(sig3):2-2/length(sig3))#Dan valor negativo, no se por que... 
A4=-trapz(abs(sig4),0:2/length(sig4):2-2/length(sig4))#... asi que les puse el "-" >:v
A5=-trapz(abs(sig5),0:2/length(sig5):2-2/length(sig5))
sound(sig1,fs1) #Se reproduce cada uno a su frecuencia de muestreo
sound(sig2,fs2)
sound(sig3,fs3)
sound(sig4,fs4)
sound(sig5,fs5)
#Segunda parte
[sig6,fs6]=audioread("record.wav"); #Es un archivo que tengo, esta muestreado a 8 kHz
fs7=4000;
fs8=6000;
fs9=12000;
fs10=16000;
sig7=resample(sig6,fs7,fs6);
sig8=resample(sig6,fs8,fs6);
sig9=resample(sig6,fs9,fs6);
sig10=resample(sig6,fs10,fs6);
duraciones=[length(sig6)/fs6,length(sig7)/fs6,length(sig8)/fs6,length(sig9)/fs6,length(sig10)/fs6]
sound(sig6,fs6) #Se reproducen todos a 8 kHz
sound(sig7,fs6)
sound(sig8,fs6)
sound(sig9,fs6)
sound(sig10,fs6)
